local ls = require 'luasnip'
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
local l = extras.lambda
local rep = extras.rep
local p = extras.partial
local m = extras.match
local n = extras.nonempty
local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local conds = require("luasnip.extras.expand_conditions")
local postfix = require("luasnip.extras.postfix").postfix
local types = require("luasnip.util.types")
local parse = require("luasnip.util.parser").parse_snippet
local ms = ls.multi_snippet

local file_header = s({
    trig = "fhdr-rysiu",
    namr = "file-hdr-rysiu",
    dscr = "Insert File Header",
}, {
    t({ "/*", " * " }),
    f(function() return { vim.fn.expand('%:t') } end, {}),
    t({ "", " * Copyright (C) " }),
    f(function() return { os.date('%Y') } end, {}),
    t({ " Marcin Osiński <osinski.marcin.r@gmail.com>",
        " *",
        " * Distributed under terms of the MIT license.",
        " */",
        "",
        "",
    }),
    c(1, {
        t("#pragma once"),
        sn(2, {
            t("#include \""),
            f(function() return { vim.fn.expand('%:t') } end, {}),
            t("\""),
        }),
    }),
    t(""),
}
)
local func_declaration = s({
    trig = "fd",
    namr = "funcdecl",
    dscr = "Insert Function Declaration",
}, {
    t(""),
    i(1, "void"),
    t(" "),
    i(2, "func"),
    t("("),
    i(3),
    t(");"),
}
)

ls.add_snippets("c", {
    file_header,
    func_declaration,
})

ls.add_snippets("cpp", {
    file_header,
    func_declaration,
    s({
        trig = "sd",
        namr = "structdecl",
        dscr = "Insert Struct Declaration",
    }, {
        t({ "",
            "struct ",
        }),
        i(1, "structname"),
        t({ "",
            "{",
            "\t",
        }),
        i(2),
        t({"", "};", ""}),
    }
    ),
    s({
        trig = "cd",
        namr = "classdecl",
        dscr = "Insert Class Declaration",
    }, {
        t({ "",
            "class ",
        }),
        i(1, "classname"),
        t({ "",
            "{",
            "  public:",
            "\t",
        }),
        i(2),
        t({ "",
            "  private:",
            "\t",
        }),
        i(3),
        t({"", "};", ""}),
    }
    ),
})
