-- Lazy Config -------------------------------------------------------------------------------------
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = "," -- Make sure to set `mapleader` before lazy so your mappings are correct

require("lazy").setup({
    "folke/neodev.nvim",
    "folke/zen-mode.nvim",
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        opts = {
            -- your configuration comes here
            -- or leave it empty to use the default settings
        }
    },

    {
        "nvim-telescope/telescope.nvim",
        dependencies = { "nvim-lua/plenary.nvim" }
    },
    "benfowler/telescope-luasnip.nvim",

    "hoob3rt/lualine.nvim",
    "sainnhe/gruvbox-material",

    "nvim-treesitter/nvim-treesitter",
    --"nvim-treesitter/playground",

    "neovim/nvim-lspconfig",
    "ray-x/lsp_signature.nvim",
    "hrsh7th/nvim-cmp",
    "hrsh7th/cmp-nvim-lsp",
    "hrsh7th/cmp-path",
    "hrsh7th/cmp-cmdline",
    "hrsh7th/cmp-buffer",
    {
        "paopaol/cmp-doxygen",
        dependencies = { "nvim-treesitter/nvim-treesitter-textobjects" }
    },

    "p00f/clangd_extensions.nvim",

    {
        "rossjaywill/insights.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope.nvim",
        },
    },

    "saadparwaiz1/cmp_luasnip",
    "L3MON4D3/LuaSnip",

    "danymat/neogen",

    {
        "lukas-reineke/indent-blankline.nvim",
        main = "ibl",
        opts = {}
    },

    {
        "christoomey/vim-tmux-navigator",
        cmd = {
            "TmuxNavigateLeft",
            "TmuxNavigateDown",
            "TmuxNavigateUp",
            "TmuxNavigateRight",
            "TmuxNavigatePrevious",
        },
        keys = {
            { "<c-h>",  "<cmd><C-U>TmuxNavigateLeft<cr>" },
            { "<c-j>",  "<cmd><C-U>TmuxNavigateDown<cr>" },
            { "<c-k>",  "<cmd><C-U>TmuxNavigateUp<cr>" },
            { "<c-l>",  "<cmd><C-U>TmuxNavigateRight<cr>" },
            { "<c-\\>", "<cmd><C-U>TmuxNavigatePrevious<cr>" },
        },
    },

    "tpope/vim-obsession",

    "sindrets/diffview.nvim",
    "nvim-tree/nvim-web-devicons",
})

----------------------------------------------------------------------------------------------------

-- Editor Settings ---------------------------------------------------------------------------------
vim.opt.mouse = ""
vim.opt.showmatch = true
vim.opt.showcmd = true
vim.opt.ignorecase = true
vim.opt.hlsearch = true
vim.opt.relativenumber = true
vim.opt.number = true
vim.opt.incsearch = true
vim.opt.cursorline = true
vim.opt.scrolloff = 5
vim.opt.autoindent = true
vim.opt.laststatus = 2
vim.opt.showmode = false
vim.opt.ruler = true

vim.opt.expandtab = true
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4

vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.colorcolumn = { 80, 100, 120 }

vim.opt.cmdheight = 2

vim.opt.termguicolors = true

vim.opt.background = 'dark'
vim.g.gruvbox_material_better_performance = 1
vim.g.gruvbox_material_foreground = 'material'
vim.g.gruvbox_material_enable_bold = 1
vim.g.gruvbox_material_enable_italic = 1
vim.cmd("colorscheme gruvbox-material")
----------------------------------------------------------------------------------------------------

-- Terminal Mappings -------------------------------------------------------------------------------
-- turn on terminal and jump into it
vim.api.nvim_set_keymap('n', '<leader>t', ':sp term://zsh<CR><C-w>J:res-10<CR>i', { silent = true })
-- turn on terminal and don't jump into it
vim.api.nvim_set_keymap('n', '<leader>T', ':sp term://zsh<CR><C-w>J:res-10<CR><C-w>k', { silent = true })
-- disable insert mode inside terminal
vim.api.nvim_set_keymap('t', '<leader>te', '<C-\\><C-n>', { silent = true })
-- jump from terminal to window above it
vim.api.nvim_set_keymap('t', '<leader>tu', '<C-\\><C-n><C-w>k', { silent = true })
-- close terminal (when inside it)
vim.api.nvim_set_keymap('t', '<leader>tq', '<C-\\><C-n>ZQ', { silent = true })
----------------------------------------------------------------------------------------------------

-- Telescope Mappings and Config -------------------------------------------------------------------
vim.api.nvim_set_keymap('n', '<leader>ff', ':Telescope find_files<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>fgf', ':Telescope git_files<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>frg', ':Telescope live_grep<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>fgs', ':Telescope grep_string<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>fb', ':Telescope buffers<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>fh', ':Telescope help_tags<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>cmd', ':Telescope commands<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>gd', ':Telescope lsp_definitions<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>gtd', ':Telescope lsp_type_definitions<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>gi', ':Telescope lsp_implementations<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>gr', ':Telescope lsp_references<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>sd', ':Telescope lsp_document_symbols<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>sw', ':Telescope lsp_workspace_symbols<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>swd', ':Telescope lsp_dynamic_workspace_symbols<CR>', { silent = true })
vim.api.nvim_set_keymap('n', '<leader>dg', ':Telescope diagnostics<CR>', { silent = true })

require('telescope').load_extension('luasnip')
require('telescope').setup {
    defaults = {
        -- Default configuration for telescope goes here:
        -- config_key = value,
        mappings = {
            i = {
                -- map actions.which_key to <C-h> (default: <C-/>)
                -- actions.which_key shows the mappings for your picker,
                -- e.g. git_{create, delete, ...}_branch for the git_branches picker
                ["<C-h>"] = "which_key"
            }
        },
        layout_strategy = 'vertical',
        layout_config = {
            bottom_pane = {
                height = 25,
                preview_cutoff = 120,
                prompt_position = "top"
            },
            center = {
                height = 0.6,
                preview_cutoff = 40,
                prompt_position = "bottom",
                width = 150
            },
            cursor = {
                height = 0.9,
                preview_cutoff = 40,
                width = 0.8
            },
            horizontal = {
                height = 0.9,
                preview_cutoff = 120,
                prompt_position = "bottom",
                width = 200,
                preview_width = 120,
            },
            vertical = {
                height = 40,
                preview_cutoff = 40,
                prompt_position = "bottom",
                width = 150,
                preview_height = 25,
            },
        }
    },
    pickers = {
        -- Default configuration for builtin pickers goes here:
        -- picker_name = {
        --   picker_config_key = value,
        --   ...
        -- }
        -- Now the picker_config_key will be applied every time you call this
        -- builtin picker
    },
    extensions = {
        -- Your extension configuration goes here:
        -- extension_name = {
        --   extension_config_key = value,
        -- }
        -- please take a look at the readme of the extension you want to configure
        luasnip = {},
    }
}
----------------------------------------------------------------------------------------------------

-- Misc Mappings -----------------------------------------------------------------------------------
-- close buffers more normal way
vim.api.nvim_set_keymap('n', '<leader>bd', ':bp|bd #<CR>', { silent = true })
-- show trailing whitespaces
vim.api.nvim_set_keymap('n', '<leader>ws', '/\\s\\+$<CR>', { silent = true })
-- turn search highlights off
vim.api.nvim_set_keymap('n', '<leader><space>', ':nohlsearch<CR>', { silent = true })
-- switch between source/header files
vim.api.nvim_set_keymap('n', '<leader>ssh', ':ClangdSwitchSourceHeader<CR>', { silent = true })
-- toggle inlay hints
vim.api.nvim_set_keymap('n', '<leader>tih',
                        ':lua vim.lsp.inlay_hint.enable(not vim.lsp.inlay_hint.is_enabled())<CR>',
                        { silent = true })
-- open diagnostic float
vim.api.nvim_set_keymap('n', '<leader>de', '<cmd>lua vim.diagnostic.open_float()<CR>', { silent = true })
----------------------------------------------------------------------------------------------------

-- Custom Functions --------------------------------------------------------------------------------
vim.api.nvim_create_user_command(
    "SetZephyrIndentation",
    function()
        vim.opt.expandtab = false
        vim.opt.tabstop = 8
        vim.opt.shiftwidth = 8
    end,
    {}
)
vim.api.nvim_create_user_command(
    "SetNormalIndentation",
    function()
        vim.opt.expandtab = true
        vim.opt.tabstop = 4
        vim.opt.shiftwidth = 4
    end,
    {}
)
----------------------------------------------------------------------------------------------------

-- Plugins Setup -----------------------------------------------------------------------------------
require('neogen').setup {
    snippet_engine = "luasnip",
}

vim.opt.list = true
vim.opt.listchars:append "space:⋅"
vim.opt.listchars:append "eol:↴"
require('ibl').setup {
    indent = {
        char = "▏",
    },
    scope = {
        show_start = false,
        show_end = false,
        priority = 1,
    },
}

require('lualine').setup {
    options = {
        icons_enabled = true,
        theme = 'gruvbox-material',
        disabled_filetypes = {}
    },
    sections = {
        lualine_a = { 'mode' },
        lualine_b = { 'branch' },
        lualine_c = { { 'filename', path = 1 } },
        lualine_x = { 'encoding', 'fileformat', 'filetype' },
        lualine_y = { 'progress' },
        lualine_z = { 'location' }
    },
    inactive_sections = {
        lualine_a = {},
        lualine_b = {},
        lualine_c = { { 'filename', path = 1 } },
        lualine_x = { 'location' },
        lualine_y = {},
        lualine_z = {}
    },
    tabline = {},
    extensions = {}
}

require('insights').setup {
    local_only = true
}
----------------------------------------------------------------------------------------------------

-- Treesitter Config -------------------------------------------------------------------------------
require('nvim-treesitter.configs').setup {
    highlight = {
        enable = true, -- false will disable the whole extension
        additional_vim_regex_highlighting = false,
    },
}
----------------------------------------------------------------------------------------------------

-- Snippets Config ---------------------------------------------------------------------------------
-- luasnip setup
local ls = require('luasnip')
ls.setup({
    keep_roots = true,
    link_roots = true,
    link_children = true,

    -- Update more often, :h events for more info.
    update_events = "TextChanged,TextChangedI",
    -- Snippets aren't automatically removed if their text is deleted.
    -- `delete_check_events` determines on which events (:h events) a check for
    -- deleted snippets is performed.
    -- This can be especially useful when `history` is enabled.
    delete_check_events = "TextChanged",
    ext_opts = {
        [require("luasnip.util.types").choiceNode] = {
            active = {
                virt_text = { { "choiceNode", "Comment" } },
            },
        },
    },
    -- treesitter-hl has 100, use something higher (default is 200).
    ext_base_prio = 300,
    -- minimal increase in priority.
    ext_prio_increase = 1,
    enable_autosnippets = false,
    -- mapping for cutting selected text so it's usable as SELECT_DEDENT,
    -- SELECT_RAW or TM_SELECTED_TEXT (mapped via xmap).
    store_selection_keys = "<Tab>",
    ft_funct = require("luasnip.extras.filetype_functions").from_cursor,
})
require("luasnip.loaders.from_lua").load({ paths = { "./snippets/" } })

-- luasnip mappings
vim.keymap.set({ "i" }, "<C-e>",
    function()
        if ls.expand_or_jumpable() then
            ls.expand()
        end
    end,
    { silent = true })

vim.keymap.set({ "i", "s" }, "<C-f>",
    function()
        if ls.jumpable(1) then
            ls.jump(1)
        end
    end,
    { silent = true })

vim.keymap.set({ "i", "s" }, "<C-d>",
    function()
        if ls.jumpable(-1) then
            ls.jump(-1)
        end
    end,
    { silent = true })

vim.keymap.set({ "i", "s" }, "<C-c>",
    function()
        if ls.choice_active() then
            ls.change_choice(1)
        end
    end,
    { silent = true })

----------------------------------------------------------------------------------------------------

-- NVIM-CMP Config ---------------------------------------------------------------------------------
-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menu,menuone,noselect'
local has_words_before = function()
    unpack = unpack or table.unpack
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local cmp = require 'cmp'
cmp.setup {
    view = {
        entries = { name = 'custom', selection_order = 'near_cursor' }
    },
    snippet = {
        expand = function(args)
            luasnip.lsp_expand(args.body)
        end,
    },
    mapping = {
        ['<C-p>'] = cmp.mapping.select_prev_item(),
        ['<C-n>'] = cmp.mapping.select_next_item(),
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.close(),
        ['<CR>'] = cmp.mapping.confirm {
            select = false,
        },
        --['<Tab>'] = function(fallback)
        --    if vim.fn.pumvisible() == 1 then
        --        vim.fn.feedkeys(vim.api.nvim_replace_termcodes('<C-n>', true, true, true), 'n')
        --    elseif luasnip.expand_or_jumpable() then
        --        vim.fn.feedkeys(vim.api.nvim_replace_termcodes('<Plug>luasnip-expand-or-jump', true, true, true), '')
        --    elseif has_words_before() then
        --        cmp.complete()
        --    else
        --        fallback()
        --    end
        --end,
        --['<S-Tab>'] = function(fallback)
        --    if vim.fn.pumvisible() == 1 then
        --        vim.fn.feedkeys(vim.api.nvim_replace_termcodes('<C-p>', true, true, true), 'n')
        --    elseif luasnip.jumpable(-1) then
        --        vim.fn.feedkeys(vim.api.nvim_replace_termcodes('<Plug>luasnip-jump-prev', true, true, true), '')
        --    else
        --        fallback()
        --    end
    },
    sources = {
        { name = 'nvim_lsp' },
        { name = 'luasnip' },
        { name = 'doxygen' },
        { name = 'path' },
        { name = 'nvim_lsp_signature_help' },
    },
    window = {
        completion = cmp.config.window.bordered(),
        documentation = cmp.config.window.bordered(),
    },
}

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
        { name = 'git' }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
    }, {
        { name = 'buffer' },
    })
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = 'buffer' }
    }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})
----------------------------------------------------------------------------------------------------

-- LSP config --------------------------------------------------------------------------------------
require('neodev').setup {}

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
    --local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

    -- Mappings.
    local opts = { noremap = true, silent = true }

    -- See `:help vim.lsp.*` for documentation on any of the below functions
    buf_set_keymap('n', '<leader>h', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
    buf_set_keymap('n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
    buf_set_keymap('n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
    buf_set_keymap('n', '<leader>fmt', '<cmd>lua vim.lsp.buf.format { async = true } <CR>', opts)
    buf_set_keymap('n', '<leader>sh', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
end

local nvim_lsp = require('lspconfig')
local servers = { 'cmake', 'bashls', 'lua_ls' }
for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup {
        on_attach = on_attach,
        flags = {
            debounce_text_changes = 150,
        },
    }
end

nvim_lsp.clangd.setup {
    on_attach = on_attach,

    cmd = {
        'clangd', '--background-index', '--clang-tidy',
        '--query-driver=**/*gcc,**/*g++,**/clang,**/clang++',
        '--completion-style=detailed',
        '--fallback-style=mozilla',
        '--enable-config',
    },

    flags = {
        debounce_text_changes = 150,
    },
}

nvim_lsp.pylsp.setup {
    on_attach = on_attach,

    settings = {
        pylsp = {
            plugins = {
                -- linter
                pylint = { enabled = false, executable = "pylint" },
                -- type checker
                pylsp_mypy = { enabled = true, live_mode = true },
                -- auto-completion options
                jedi_completion = { fuzzy = true },
                -- unused plugins
                rope_autoimport = { enabled = false, },
                rope_completion = { enabled = false, },
                autopep8 = { enabled = false, },
                flake8 = { enabled = false, },
                pycodestyle = { enabled = false, },
                pydocstyle = { enabled = false, },
                mccabe = { enabled = false, },
                yapf = { enabled = false, },
                ruff = { enabled = false, },
            }
        }
    },

    flags = {
        debounce_text_changes = 150,
    },
}

-- extra python linter
nvim_lsp.ruff.setup {
    on_attach = on_attach,

    init_options = {
        settings = {
            configurationPreference = "filesystemFirst",
            lineLength = 100,
            fixAll = true,
            organizeImports = true,
            showSyntaxErrors = true,
            codeAction = {
                disableRuleComment = { enable = false },
                fixViolation = { enable = false },
            },
            lint = {
                select = { "E", "F", "B", "UP", "SIM", "I" },
                ignore = {},
            },
        }
    },

    flags = {
        debounce_text_changes = 150,
    },
}

nvim_lsp.gopls.setup {
    on_attach = on_attach,

    settings = {
        gopls = {
            analyses = {
                unusedparams = true,
                unreachable = true,
                shadow = true,
            },
            completeUnimported = true,
            usePlaceholders = true,
            gofumpt = true,
        }
    },

    flags = {
        debounce_text_changes = 150,
    },
}

vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(
    vim.lsp.handlers.hover,
    { border = 'rounded' }
)

vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(
    vim.lsp.handlers.signature_help,
    { border = 'rounded' }
)
----------------------------------------------------------------------------------------------------
