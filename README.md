# How to use

Make sure GNU Stow is installed.

Clone this repo (remember to add `--recurse-submodules` flag to initialize submodules with zsh
plugins) to `~/.dotfiles` `cd` into it and run `stow --dir=. --target=../.config .` to make Stow 
create symlinks pointing to contents of this repo in `~/.config` folder
