# vim: filetype=sh

## History command configuration
HISTFILE=$HOME/.config/zsh/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt INC_APPEND_HISTORY_TIME

export EDITOR='nvim'

# Compilation flags
export ARCHFLAGS="-arch x86_64"

alias zshconfig="nvim $XDG_CONFIG_HOME/zsh/.zshrc"
alias vimconfig="nvim $XDG_CONFIG_HOME/nvim/init.lua"
alias swayconfig="nvim $XDG_CONFIG_HOME/sway/config"
alias hyprconfig="nvim $XDG_CONFIG_HOME/hypr/hyprland.conf"

alias cp="cp -iv"
alias mv="mv -iv"
alias df="df -h"
alias free="free -m"
alias more=less
alias ll="eza -lgh"
alias lla="eza -algh"
alias llt="eza -algh --tree"
alias llcode="eza --git --git-ignore --tree --color=always -alh"
alias cat=bat
alias vim="nvim"
alias nvimdiff="nvim -d"
alias vimdiff=nvimdiff
alias term="alacritty & disown"
alias ipy=ipython

alias weather="curl wttr.in"
alias myip="curl ifonfig.co"
alias parrot="curl parrot.live"

 
zstyle ':completion:*' list-colors

# Enable colors and change prompt:
autoload -U colors && colors	# Load colors
PS1="%B%{$fg[yellow]%}[%{$fg[blue]%}%2~%{$fg[yellow]%}]%{$reset_color%}$%b "
setopt autocd		# Automatically cd into typed directory.
setopt interactive_comments

# Display GIT info in RPROMPT
autoload -Uz vcs_info
precmd_vcs_info() { vcs_info }
precmd_functions+=( precmd_vcs_info )
setopt prompt_subst
RPROMPT='%B%{$fg[blue]%}${vcs_info_msg_0_}%{$reset_color%}%b'
zstyle ':vcs_info:git:*' formats       '(%b)'
zstyle ':vcs_info:git:*' actionformats '(%b-%a)'

# Basic auto/tab complete:
unsetopt menu_complete
unsetopt flowcontrol
setopt auto_menu
setopt complete_in_word
setopt always_to_end
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
zmodload zsh/complist
_comp_options+=(globdots)		# Include hidden files.
autoload -U compinit && compinit

# vi mode
bindkey -v
export KEYTIMEOUT=1

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# Change cursor shape for different vi modes.
function zle-keymap-select () {
    case $KEYMAP in
        vicmd) echo -ne '\e[1 q';;      # block
        viins|main) echo -ne '\e[5 q';; # beam
    esac
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line
bindkey -M vicmd '^[[P' vi-delete-char
bindkey -M vicmd '^e' edit-command-line
bindkey -M visual '^[[P' vi-delete

# Better history search with arrow kids
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search # Up
bindkey "^[[B" down-line-or-beginning-search # Down

source $XDG_CONFIG_HOME/zsh/plugins/zsh-completions/zsh-completions.plugin.zsh
#source $XDG_CONFIG_HOME/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $XDG_CONFIG_HOME/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/fzf/key-bindings.zsh

source $XDG_CONFIG_HOME/broot/launcher/bash/br

source $XDG_CONFIG_HOME/zsh/useful-functions.zsh

